#!/usr/bin/env python

from __future__ import print_function
import struct
import binascii

byte2hex = binascii.b2a_hex

try:
    range = xrange
except NameError:
    pass

def _left_rotate(n, b):
    return ((n << b) | (n >> (32 - b))) & 0xffffffff

class SHA1(object):
    def __init__(s, message):
        s.message = message

        s.original_byte_len = len(s.message)
        s.original_bit_len = s.original_byte_len * 8

        # Initialize variables:
        s.h0 = 0x67452301
        s.h1 = 0xEFCDAB89
        s.h2 = 0x98BADCFE
        s.h3 = 0x10325476
        s.h4 = 0xC3D2E1F0

    def set_state(s, x):
        s.h0, s.h1, s.h2, s.h3, s.h4 = x

    def padding(self):
        return struct.pack(b'>Q', self.original_bit_len)

    def preproc1(self):

        # append the bit '1' to the message
        extra = b'\x80'

        # append 0 <= k < 512 bits '0', so that the resulting message length (in bits)
        #    is congruent to 448 (mod 512)
        extra += b'\x00' * ((56 - (self.original_byte_len + 1) % 64) % 64)

        return extra

    @staticmethod
    def fromstate(hexdigest):
        assert len(hexdigest) == 160 / 8 * 2
        part = len(hexdigest) / 5

        return tuple([int( hexdigest[part*i:part*(i+1)], 16 ) for i in range(5)])


    def run(s):
        """SHA-1 Hashing Function

        A custom SHA-1 hashing function implemented entirely in Python.

        Arguments:
            message: The input message string to hash.

        Returns:
            A hex SHA-1 digest of the input message.
        """

        # Pre-processing:
        s.message += s.preproc1()

        # append length of message (before pre-processing), in bits, as 64-bit big-endian integer
        s.message += s.padding()
        message = s.message
        print('len', len(message))

        # Process the message in successive 512-bit chunks:
        # break message into 512-bit chunks
        for i in range(0, len(message), 64):
            w = [0] * 80
            # break chunk into sixteen 32-bit big-endian words w[i]
            for j in range(16):
                w[j] = struct.unpack(b'>I', message[i + j*4:i + j*4 + 4])[0]
            # Extend the sixteen 32-bit words into eighty 32-bit words:
            for j in range(16, 80):
                w[j] = _left_rotate(w[j-3] ^ w[j-8] ^ w[j-14] ^ w[j-16], 1)

            # Initialize hash value for this chunk:
            a = s.h0
            b = s.h1
            c = s.h2
            d = s.h3
            e = s.h4

            for i in range(80):
                if 0 <= i <= 19:
                    # Use alternative 1 for f from FIPS PB 180-1 to avoid ~
                    f = d ^ (b & (c ^ d))
                    k = 0x5A827999
                elif 20 <= i <= 39:
                    f = b ^ c ^ d
                    k = 0x6ED9EBA1
                elif 40 <= i <= 59:
                    f = (b & c) | (b & d) | (c & d)
                    k = 0x8F1BBCDC
                elif 60 <= i <= 79:
                    f = b ^ c ^ d
                    k = 0xCA62C1D6

                a, b, c, d, e = ((_left_rotate(a, 5) + f + e + k + w[i]) & 0xffffffff,
                                a, _left_rotate(b, 30), c, d)

            # sAdd this chunk's hash to result so far:
            s.h0 = (s.h0 + a) & 0xffffffff
            s.h1 = (s.h1 + b) & 0xffffffff
            s.h2 = (s.h2 + c) & 0xffffffff
            s.h3 = (s.h3 + d) & 0xffffffff
            s.h4 = (s.h4 + e) & 0xffffffff

        # Produce the final hash value (big-endian):
        return '%08x%08x%08x%08x%08x' % (s.h0, s.h1, s.h2, s.h3, s.h4)

if __name__ == '__main__':
    # Imports required for command line parsing. No need for these elsewhere
    import argparse
    import sys
    import os
    import hashlib

    #key = 'double hopking buuking cows'
    #msg = 'we strike at midnight'
    #msg2 = 'but only if the bird sings'

    key = 'doppelganger'
    msg = 'comment1=cooking%20MCs;userdata=foo;comment2=%20like%20a%20pound%20of%20bacon'
    msg2 = ';admin=true'
    keymsg = key + msg

    s = SHA1(keymsg)
    hm = s.run()

    for kl in range(1, 64):
        try:
            gs = SHA1(' ' * (kl + len(msg))) # to get the glue padding
            glue_pad = gs.preproc1() + gs.padding()

            #print('glue_pad', byte2hex(glue_pad))
            #print('keymsg', byte2hex(s.message))

			# TODO: fix this, at this point keymsg is unknown
			# so instead we should only pass the message length
            s2 = SHA1(keymsg + glue_pad + msg2)
            s2.set_state(SHA1.fromstate(hm))
            s2.message = msg2
            hm2 = s2.run()

            re = hashlib.sha1(keymsg).hexdigest()
            re2 = hashlib.sha1(keymsg + glue_pad + msg2).hexdigest()

            assert hm == re, '{} == {}'.format(hm, re)
            #assert hm2 == re2, '{} == {}'.format(hm2, re2)
            if hm2 == re2:
                print('win!', kl)
                break

        except struct.error:
            print('err')
            pass

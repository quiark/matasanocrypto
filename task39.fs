﻿module task39

open Program

open System
open System.Numerics
open System.Security.Cryptography
open System.Globalization

open FSharp.Collections.ParallelSeq
open task33


let rec allprimes10 top =
    seq {
        yield 2
        let cache = allprimes10 top |> Seq.cache
        let isprime n =
            cache |> Seq.takeWhile (fun i->i*i<=n) |> Seq.forall (fun i->n%i<>0)
        yield! {3 .. 2 .. top } |> Seq.filter isprime
    }

let prime_exhaustive_test x =
    match Seq.tryFind (fun y -> (x % y) = 0I) (seq { 2I .. (x - 1I) }) with
    | Some x -> false
    | None -> true

// returns true if passed, i.e. probably prime
let Fermat_test n k =
    // big_rand is probably not the best implementation in here but...
    let candidates = seq { for i in { 0 .. k } do yield 2I + big_rand (n - (bigint 4)) }
    Seq.exists (fun a -> (modpow a (n - 1I) n) <> 1I) candidates 
    |> not

// also returns true if probably prime
let Miler_Rabin_test n k =
    let nl1 = n - 1I
    //let nl1_bits = (nl1.ToByteArray().Length * 8)
    let rec factors2 x i =
        let rem = ref (bigint 0)
        let u = BigInteger.DivRem(x, 2I, rem)
        match (!rem).IsZero with
        | true -> factors2 u (i + 1)
        | _ -> (i, x)

    // returns true if composite
    let rec sqtest x r =
        match r with
        | 0 -> true
        | 1 -> true
        | _ ->  let x' = modpow x 2I n
                if x' = 1I then true
                else if x' = nl1 then false
                else sqtest x' (r - 1)

    let r, d = factors2 nl1 0
    let candidates = seq { for i in { 0 .. k } do yield 2I + big_rand (n - (bigint 4)) }
    Seq.exists (fun a ->        // predicate returns true --> n is composite
        let x = modpow a d n
        if (x = 1I) || (x = nl1) then false
        else sqtest x r
        ) candidates
    |> not

let inline (-->) a b = b || (not a)

let isprime_simple p =
    (Fermat_test p 500) && (Miler_Rabin_test p 500)

let rec big_rand_prime max =
    let p = big_rand max
    if (Fermat_test p 100) && (Miler_Rabin_test p 100)
    then p
    else big_rand_prime max

let tests_test () =
    let top = 100 * 1000
    let primes = Set.ofSeq (allprimes10 top)
    Seq.find (fun (x:int) -> 
        let ts1 () = Fermat_test (bigint x) 10000
        let ts2 () = Miler_Rabin_test (bigint x) 10000
        let in_primes = (Set.contains x primes)
        let prime_test () = (ts1() && ts2())
        not (in_primes = prime_test ())
    ) { 5 .. top }


let rec egcd (a:bigint) (b:bigint): bigint * bigint * bigint =
    match a.IsZero with
    | true -> (b, 0I, 1I)
    | false -> let (g, s, t) = egcd (b % a) a
               (g, (t - (b / a) * s), s) 

let egcd_test (a:int) (b:int) = 
    let g, x, y = (egcd (bigint a) (bigint b))
    assert ((bigint a) * x + (bigint b) * y = g)

let unnegative (a:bigint) (n:bigint) =
    let x = a % n
    if x >= 0I then x else x + n

let invmod a m =
    let gcd, x, y = egcd a m
    if gcd.IsOne then unnegative x m
    else raise (ArgumentException ("Does not have inverse."))

let eqmod a b m =
    (a % m) = (b % m)

let RSA (p:bigint) (q:bigint) (pl:bigint) =
            //assert (prime_exhaustive_test p)
            //assert (prime_exhaustive_test q)
            let n = p * q
            let e = bigint 3
            let et = (p - 1I) * (q - 1I)
            let d = invmod e et
            let cr = modpow pl e n
            (cr, d, n)


let RSA_decrypt (p:bigint) (q:bigint) (cr:bigint) (d:bigint) =
    let n = p * q
    let pl = modpow cr d n
    pl

// Stolen from http://gettingsharper.de/2012/01/24/the-chinese-remainder-theorem/
/// chin. remainder theorem  solve X = b_i (mod n_i)  for t natural numbers n_i,
/// beeing pairwise relative prime
let chinese_remainder_raw (a's:bigint list) (ns:bigint list) =
    let getMs a b = egcd a b |> (fun (_,_,s) -> s)
    let N = ns |> List.fold (*) 1I
    let Ns:bigint list = ns |> List.map (fun n -> N/n)
    let es = List.map2 getMs ns Ns
    let As = List.map3 (fun a b c -> a * b * c) a's es Ns
    List.sum As

let chinese_remainder (a's:bigint list) (ns:bigint list) =
    let N = ns |> List.fold (*) 1I
    unnegative (chinese_remainder_raw a's ns) N

let crt_test () =
    let ck a's ns exp =
        let act = chinese_remainder a's ns
        assert (act = exp)

        Seq.iter2 (fun a n -> assert (eqmod act a n)) a's ns

    ck [0I; 2I; 4I] [2I; 3I; 5I] 14I
    ck [2I; 3I; 1I] [3I; 4I; 5I] 11I
    ck [2I; 3I; 2I] [3I; 5I; 7I] 23I

let task39 () = 
    let try_p = fun (p_:int) ->
        try
            printf "%A .. " p_
            let q = bigint 29
            let p = bigint p_
            let pl = bigint 4
            let (cr, d, n) = RSA p q pl
            let pl2 = modpow cr d n
            (pl2 = pl) |> printf "%A\n"
        with
            :? ArgumentException -> ()

    Seq.iter try_p (allprimes10 100) |> ignore
    //try_p 3
    //true

    //tests_test () |> printf "Found problem at %A"

let cube_root (x:bigint) =
    let rec cube_root_raw x s y : bigint =
        if s >= 0 then
            let y = y + y
            let b:bigint = 3I * y * (y + 1I) + 1I
            if (x >>> s) >= b then
                cube_root_raw (x - (b <<< s)) (s - 3) (y + 1I)
            else
                cube_root_raw x (s - 3) y
        else
            y

    let bits = Math.Ceiling(BigInteger.Log(BigInteger.Abs(x), 2.0)) * 3.0 + 6.0 |> int
    //printf "x=%A bits=%A\n" x bits
    let res = cube_root_raw (BigInteger.Abs x) bits 0I
    if x < 0I then 
        -res
    else
        res
    
let cube_root_test () =
    let ck CR =
        let x = CR * CR * CR
        assert (CR = (cube_root x))

    ck 26765050224239095736491I
    //List.iter ck [1 .. 2642245]


let task40 () = 
    let pl = bigint 14
    let keypairs = [ RSA (bigint 11) (bigint 3) pl ; RSA (bigint 23) (bigint 479) pl ; RSA (bigint 29) (bigint 41) pl ]
    let ns = (List.map (fun (_,_,n) -> n) keypairs)
    let c's = (List.map (fun (c,_,_) -> c) keypairs)
    let N = List.fold (*) 1I ns
    let p3 = chinese_remainder_raw c's ns
    let p3N = unnegative p3 N
    
    Seq.iter (fun i -> assert (eqmod p3N c's.[i] ns.[i])) [0 .. 2]
    unnegative (cube_root p3N) (List.min ns) |> printf "pl=%A\n"

let task41 () =
    let pl = big_rand 100I
    let E = 3I
    let p, q = 479I, 23I
    let C, d, N = RSA p q pl
    let S = (big_rand (N - 2I)) + 2I
    let C' = (modpow S E N) * C
    let P' = modpow C' d N
    let pl' = modpow C d N
    let P = (P' * (invmod S N)) % N
    assert (P = pl)
    
let RSA_keygen max =
    let p = big_rand_prime max
    
    let rec calc_q () =
        let q = big_rand_prime max
        try
            let _, d, n = RSA p q 1I
            q
        with :? ArgumentException ->
                calc_q () // try next one

    (p, calc_q ())

let task42 () =
    let max = BigInteger.Pow(2I, 512)
    // signature should be 1024bit, I'll take it as the length of n
    // that means we have p and q be 512b so they multiplied make 512 + 512

    // now verification of the signature will cube it, tripling the bit length
    // we need to stay within 1024b so signature should be around 1024 / 3 = 340b

    // not all numbers have an integer cube root obviously so when forging the
    // signature, the end of the number is not under my control. Therefore I must
    // put as much of searched byte string in the front (most significant digits)
    // as possible

    let p = "9894656735403306267126018344853584214723051414102962583603543606582289786952396984698239449756109300113849295978314611914915087502296847491500072273780141" |> BigInteger.Parse
    let q = "5428185182127390652029298741304213766647769853272413113383174713548907051802175264248517154536642362879529233882733773577815137092237304825240379591635663" |> BigInteger.Parse
    //let p, q = RSA_keygen max
    let e = 3I
    let _, d, n = RSA p q 1I   // just used to generate keypair

    // bigint <-> bytes conversion functions because we need to use the other ordering to execute the attack
    let bigint_bytes (x:bigint) =
        x.ToByteArray()
        |> Array.rev

    let bytes_bigint (x:byte array) =
        x
        |> Array.rev
        |> bigint

    // sender
    let RSA_sign (msg:bigint) =
        assert (BigInteger.Abs(msg) < n)
        let signature = modpow msg d n
        let verified = (modpow signature e n) = msg
        assert verified
        signature

    // sender
    let pad_msg (msg:String) blocksize =
        let digest = String_bytes msg |> sha1
        let ffs = Array.create (blocksize - 2 - digest.Length - 1 - 4) 0xFFuy
        let padded = Array.concat [ [| 0uy; 1uy; |]; ffs; [| 0uy |]; String_bytes "DERp";  digest; [| 0uy |] ]
        bytes_bigint padded
    let bytes_find where (what:byte array) =
        let same_substr (a:'a array) (b:'a array) aix bix ln =
            let w = Seq.map (fun i -> a.[aix + i] = b.[bix + i]) { 1..ln-1 } |> Seq.tryFindIndex not
            Option.isNone w
        let occurs = Seq.mapi (fun ix el ->
            if el = what.[0]
            then same_substr where what ix 0 (Array.length what)
            else false) where
        Seq.exists id occurs

    // receiver
    let verify_parse (sign:bigint) (msg:string) =
        let bytes = bigint_bytes (modpow sign e n)
        let digest = String_bytes msg |> sha1
        bytes_find bytes (Array.append (String_bytes "DERp") digest)

    // test
    let msg = "hi mom"
    let signature = RSA_sign (pad_msg msg 32)
    verify_parse signature msg |> printf "own sig accepted: %A\n"

    // attacker (doesn't know d)
    let digest = String_bytes msg |> sha1
    let fakesig_3pow_left = Array.concat [ [| 0uy; 1uy; |]; Array.create 3 0xFFuy ; String_bytes "DERp"; digest; [| 0uy |]]
    let fakesig_3pow = Array.append fakesig_3pow_left (Array.create (127 - (Array.length fakesig_3pow_left)) 0xAAuy)
    let fakesig = cube_root (bytes_bigint fakesig_3pow)
    let accept = verify_parse fakesig msg 
    printf "faked sig accepted: %A\n" accept
    accept
            
    (*
    let hex_bigint (s:string) =
        let rev_s =
            s
            |> seq_chop2 2
            |> Seq.map (fun x ->
                    Byte.Parse(new String(x), NumberStyles.HexNumber))
            |> Array.ofSeq
            //|> Array.rev
            |> bigint
        //printf "reversed=%A" rev_s
        rev_s
        *)

// ------ DSA ---
let L = 512
let N = 64

let rec gen_q max =
    big_rand_prime max

let rec gen_p max q m =
    if m >= 100I then Option.None
    else
        let x = (m * q) + 1I
        if isprime_simple x
        then Option.Some x
        else gen_p max q (m + 1I)

let rec gen_both () =
    let q = gen_q (BigInteger.Pow(2I, N))
    let p = gen_p (BigInteger.Pow(2I, L)) q 6I
    match p with
    | Option.Some pv -> (pv, q)
    | Option.None -> gen_both ()

let rec gen_g p q h =
    if h >= 10I then raise (ArgumentException ("Cant generate h"))
    else
        let g = modpow h ((p - 1I) / q) p
        if g = 1I then gen_g p q (h + 1I)
        else g

let p, q = gen_both ()


let check_params =
    ((isprime_simple p) && (isprime_simple q) && (((p - 1I) % q) = 0I) && ((modpow g q p) = 1I))



let try_gensig g k x (m:byte array) =
    let r = (modpow g k p) % q
    if r = 0I then Option.None
    else
        let hm = sha256 m |> bigint
        let s = ((invmod k q) * (hm + x * r)) % q
        if s = 0I then Option.None
        else Option.Some (r, s)

let rec gensig g k x m =
        try
            let v = try_gensig g k x m
            if Option.isSome v
            then Option.get v
            else gensig g k x m
        with
            :? ArgumentException -> gensig g k x m

// k, x used for debugging purposes
let verify g k (x, y) (r, s) m =
    if (r < 0I) || (r >= q) || (s < 0I) || (s >= q) then false
    else
        let w = invmod s q
        assert ((w * s) % q = 1I)
        let hm = unnegative (sha256 m |> bigint) q
        let u1 = (hm * w) % q
        let u2 = (r * w) % q
        let v = ((modpow g u1 p) * (modpow y u2 p)) % p % q
        assert (u2 = (r * k * (invmod (hm + x * r) q)) % q)
        assert (u1 = (hm * k * (invmod (hm + x * r) q)) % q)

        // ---- proof -----
        let v1 = (modpow g u1 p) * (modpow y u2 p)
        let v2 = (modpow g u1 p) * (modpow g (x * u2) p)
        let v3 = (modpow g (u1 + (x*u2)) p)
        let c = invmod (hm + x*r) q
        let v4 = (modpow g (hm*k * c + x*r*k * c) p)
        let v5 = (modpow g (k * (hm + x*r) * c) p)
        let v6 = (modpow g k p)
        assert (p > q)
        //let invalid_ix = (Seq.findIndex (fun i -> not (eqmod v1 i q)) [v; v1; v2; v3; v4; v5; v6; r] )
        //printf "v=%A\nr=%A\ninvalid_ix=%A\n" v r invalid_ix

        // -----
        eqmod v r q

let dsa_keygen g =
    let x = 1I + (big_rand (q - 1I))
    let y = modpow g x p
    x, y

let dsa_recover_x (r, s) k m =
    let hm = sha256 m |> bigint
    let ir = invmod r q
    ((unnegative (s * k - hm) q) * ir) % q

let task43 () =
    let g = gen_g p q 2I
    let x, y = dsa_keygen g
    let k_max = 2I ** 16
    let k = big_rand k_max

    printf "p=%A\nq=%A\ng=%A\nparams=%A\nx=%A\ny=%A\n" p q g check_params x y

    let find_k m (r, s) =
        let hm = sha256 m |> bigint
        // just brute-force, comparing with r
        Seq.find (fun x -> r = (modpow g x p) % q) { 0I .. k_max }


    let m = (String_bytes "attack at noon")
    let si = gensig g k x m
    //printf "sign=%A\n" si
    verify g k (x, y) si m |> printf "\nverify=%A\n.done.\n"

    let k1 = find_k m si
    let x1 = dsa_recover_x si k1 m
    assert (k1 = k)
    assert (x1 = x)


let task44 () =
    let x, y = dsa_keygen g

    let genmsg ix =
        let k = 1234I + bigint(ix % 4)
        let m = 
            sprintf "cooking %A like a pound of tofu" ix 
            |> String_bytes
        (gensig g k x m), (bigint (sha256 m))

    let msgs = 
        List.init 24 genmsg
        |> List.groupBy (fun ((r, s), _) -> r)

    let (r1, s1), m1 = (snd msgs.[0]).[0]
    let (r2, s2), m2 = (snd msgs.[0]).[1]
    assert (r1 = r2)
    let k1 = unnegative ((m1 - m2) * (invmod (s1 - s2) q)) q
    let x1 = unnegative ((m1*s2 - m2*s1)*(invmod ((s1 - s2)*r1) q)) q
    assert (x = x1)

let task45 () =
    let partA () =
        let g = 0I
        let x, y = dsa_keygen g
        let k = big_rand q
        let m = "baba banana" |> String_bytes
        let hm = m |> sha256 |> bigint
        let r = modpow g k p
        assert (r = 0I)
        let s = unnegative ((invmod k q) * (hm + x * r)) q
        // -- verify
        let ok = verify g k (x, y) (r,s) m
        ok

    let partB () =
        let g = p + 1I
        let x, y = dsa_keygen g
        let k = big_rand q
        let r,s = gensig g k x (String_bytes "abcd")
        assert (r = 1I)
        //let ok = verify g k (x,y) (r,s) (String_bytes "defg")
        let w = invmod s q
        //assert ((w * s) % q = 1I)
        let hm = unnegative ("dddd" |> String_bytes |> sha256 |> bigint) q
        let u1 = (hm * w) % q
        let u2 = (r * w) % q
        let v = ((modpow g u1 p) * (modpow y u2 p)) % p % q
        let ok = v = r
        ok

    assert (partA ())
    assert (partB ())


let task46 () =
    let max = BigInteger.Pow(2I, 1024)
    let e = 3I
    let p = BigInteger.Parse("00e58210c103e7c688c2ddac1f80cf1973bfcebe9bbebde080070604f3f094e6970e4af6e8edfede89409ec87426fd94129b31fb64c40e30fc4bfd0f66c5b744df", NumberStyles.HexNumber)
    let q = BigInteger.Parse("00e46cf0ece62f143638f2068e549eb4d1f19c3634578a66e9ad4f96f0fa062aefb9e99ba0d4f0ada84c87822ce689409aa12270dd2baa00fa7abdf6271519e7af", NumberStyles.HexNumber)
    //assert (isprime_simple p)
    //assert (isprime_simple q)

    // my code takes approx. forever to generate 1024 p, q....
    //let p, q = RSA_keygen max
    let _, d, n = RSA p q 1I   // just used to generate keypair
    
    let oracle cr =
        let pl = RSA_decrypt p q cr d
        pl.IsEven

    let b64pl = "VGhhdCdzIHdoeSBJIGZvdW5kIHlvdSBkb24ndCBwbGF5IGFyb3VuZCB3aXRoIHRoZSBGdW5reSBDb2xkIE1lZGluYQ=="
    let pl =  new BigInteger(base64_decode b64pl)
    //let pl = new BigInteger(String_bytes "Thanks for watching. Haxx0red 1337 crack like a boss. This is a secret RSA encrypted message.")
    assert (pl < n)
    let cr, _, _ = RSA p q pl

    let rec try_decrypt L (U:bigint) depth cr =
        // don't need to `mod n` here, RSA_decrypt will do it
        let printable b =
            if (b >= 32uy) && (b <= 123uy)
            then b
            else 46uy
        Console.SetCursorPosition(0, 0);
        U.ToByteArray() |> Array.map printable |> bytes_String |> printf "%A\n"
        if ((U - L) <= 0I) 
        then U
        else
            //assert ((pl >= L) && (pl <= U))
            let cr' = cr * (modpow (2I*depth) e n)   // depth starts from 1
            //let pl' = (pl * depth) % n
            //let cheatHO = pl' < (n / 2I)
            let ev = oracle cr'
            let d' = depth * 2I
            //assert (ev = cheatHO)
            if ev
            then 
                 printf "even, pl < half: U = L + n/%A\n" depth
                 //assert (( (pl >= L) && (pl <= (L + n/depth)) ) = cheatHO)
                 //if not cheatHO
                 //then printf "oops\n"
                 try_decrypt L (L + n/d') d' cr
            else 
                 printf "odd , pl > half: L = U - n/%A\n" depth
                 //assert (( (pl <= U) && (pl >= (U - n/depth)) ) = not cheatHO)
                 //if cheatHO
                 //then printf "oops\n"
                 try_decrypt (U - n/d') U d' cr

    try_decrypt 0I n 1I cr

    


task46 () 
Console.ReadLine() |> ignore

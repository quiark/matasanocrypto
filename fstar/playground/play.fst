module Play
open FStar


type Message = string
type PlName = p:string {(p = "b") \/ (p = "c")}


let b_state = ref ("")
let c_state = ref ("")


val sel_state: plid:PlName -> Tot (m:ref Message)
let sel_state plid =
	if plid = "b"
	then b_state
	else c_state



//  to -> m
new type broadcast_sent: PlName -> Message -> Type


// Definition of what it means that broadcast is honest. We send the same
// message to both participants and only send one message (maybe repeatedly).
new type BroadcastHonest = 
	(forall m. (broadcast_sent "b" m) <==> (broadcast_sent "c" m))
	/\ (forall m1 m2. (broadcast_sent "b" m1) /\ (broadcast_sent "b" m2) ==> (m1 = m2))

assume val a_send: t:PlName -> Tot (m:Message{ 
			true // BroadcastHonest
			/\ broadcast_sent "b" m 
			/\ broadcast_sent "c" m
			/\ (forall m'. (broadcast_sent "c" m' ==> m = m'))
			/\ (forall m'. (broadcast_sent "b" m' ==> m = m'))
			})


val bc_receive: plid:PlName -> m:Message -> ST (u:unit)
	(requires (fun h -> 
		   Heap.contains h b_state
		/\ b_state <> c_state
		/\ Heap.contains h c_state
	))
	(ensures (fun h0 _ h1 -> 
		// player honestly saves that value
		   (Heap.sel h1 (sel_state plid)) = m

		// required fluff
		/\ (Heap.modifies (Heap.only (sel_state plid)) h0 h1)
		/\ (Heap.contains h1 b_state)
		/\ (Heap.contains h1 c_state)
	))
let bc_receive plid m =
	let state = sel_state plid in
	state := m

// sends (by returning) the value 'plid' has received earlier
// that value is retrieved from the heap
val bc_send_compare: plid:PlName -> ST (m:Message)
	(requires (fun h -> 
		    Heap.contains h c_state
		 /\ Heap.contains h b_state
		
	))
	(ensures (fun h0 m h1 -> 
		// we are sending what we have, being honest here
		// could also use heap as source of m
		   m = (Heap.sel h0 (sel_state plid))
		/\ Heap.modifies Set.empty h0 h1
		/\ h0 = h1

		// required fluff
		/\ Heap.contains h1 b_state
		/\ Heap.contains h1 c_state
	))
let bc_send_compare plid =
	let state = sel_state plid in
	!state


// 't' is receiving 'v' from 'f' and will compare with its storage
// I wanna express that (my_val = v) <==> brodcast was honest
val bc_recv_compare: f:PlName -> t:PlName -> v:Message -> ST (r:bool)
	(requires (fun h -> 
		   broadcast_sent f v
		/\ broadcast_sent t (Heap.sel h (sel_state t))
		/\ t <> f
		/\ (forall m1 m2 . (broadcast_sent f m1 /\ broadcast_sent f m2) ==> m1 = m2)
		/\ (forall m1 m2 . (broadcast_sent t m1 /\ broadcast_sent t m2) ==> m1 = m2)
	))
	(ensures (fun h0 r h1 -> 
		   Heap.modifies Set.empty h0 h1
		/\ h0 = h1
		/\ Heap.equal h0 h1
		/\ r = (v = (Heap.sel h1 (sel_state t)))
		/\ (b2t(r) ==> (broadcast_sent "b" v) /\ (broadcast_sent "c" v))
		/\ (b2t(r) ==> BroadcastHonest)
	))
let bc_recv_compare f t v =
	let state = sel_state t in
	let my_val = !state in
	my_val = v



val main: unit -> ST (unit)
	(requires (fun h -> 
		Heap.contains h b_state
		/\ Heap.contains h c_state
		/\ b_state <> c_state
	))
	(ensures (fun h0 _ h1 -> 
		true
		))
let main () =
	//  the |> operator breaks some preconditions
	//(a_send "b") |> broadcast bc_receive;

	bc_receive "b" (a_send "b");
	bc_receive "c" (a_send "c");


	let xa = bc_send_compare "b" in
	let ya = bc_recv_compare "b" "c" xa in
	let xb = bc_send_compare "c" in
	let yb = bc_recv_compare "c" "b" xb in
	()


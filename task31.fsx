module task31

#I "/Users/roman/Projects/matasanocrypto"
#r "packages/FSharp.Collections.ParallelSeq.1.0.2/lib/net40/FSharp.Collections.ParallelSeq.dll"
#r "packages/FsCheck.0.9.4.0/lib/net40-Client/FsCheck.dll"
#r "packages/ExtCore.0.8.45/lib/net40/ExtCore.dll"
#r "packages/Suave.0.29.1/lib/net40/Suave.dll"

open Core.ExtraTopLevelOperators

#load "DataStruct.fs" "/Users/roman/Projects/matasanocrypto/Program.fs"
open Program


// In[4]:

open System.Security.Cryptography
open System
open System.Net
open System.Threading
open Suave                 // always open suave
open Suave.Http.Successful // for OK-result
open Suave.Web             // for config
open Suave.Http.Applicatives
open Suave.Http
open Suave.Model.Binding
open FSharp.Collections.ParallelSeq


let h = new HMACSHA1()


// In[5]:

h.Key <- String_bytes "abcdef"
h.Initialize()
h.ComputeHash(String_bytes  "dede") |> Array.length


// In[6]:

let insecure_compare a b =
    if (Array.length a) <> (Array.length b) then false
    else
        Seq.zip a b
        |> Seq.exists (fun (a,b) -> 
                    System.Threading.Thread.Sleep(50)
                    a <> b)
        |> not

// In[8]:

let key = String_bytes "heptometer"


// In[9]:

let verify_request (file:byte[]) (signature:byte[]) =
    let hmac = new HMACSHA1()
    hmac.Key <- key
    let true_hmac = hmac.ComputeHash(file)
    insecure_compare signature true_hmac


// In[10]:

let known = 
    [| 107; 70; 202; 15; 17; 13; 112; 21; 77; 52; 15; 161; 154; 112; 20; 216; 8; 119 |] 
    |> Seq.map byte
    |> Seq.take 10
    |> Array.ofSeq


// In[11]:

let run_request (file:string) (signature:byte[]) =
    try
        let cl = new WebClient()
        cl.DownloadString( String.Format(
                            "http://localhost:8083/file/{0}/{1}", 
                            file, 
                            signature |> hex_encode) ) |> ignore
    with 
        _ -> ()


// In[12]:

let rec backtrack_find (testfn: int -> byte -> bool) (i:int) =
    if i = 20 then true
    else
        let tryfn (b:byte): bool =
            if testfn i b then backtrack_find testfn (i + 1)
            else false
        let src:byte seq = {0uy .. 255uy}

        Seq.tryFind tryfn src
        |> Option.isSome



// In[13]:

let find_hmac file =
    let hlen = 20
    let try_one i (x:byte[]) =
        let start = DateTime.Now.Ticks
        run_request file x
        let stop = DateTime.Now.Ticks
        let interval = stop - start |> float
        let pause = 10000 * 50 + (i * 1400) |> float
        let steps = interval / pause
        printf "%si = %A steps = %A x = %A, interval = %A\n" (String.init i (fun _ -> " ")) i steps x.[i] interval
        steps
    
    let try_one_redundant i x =
        let cnt = ((i / 6) + 1) * 2
        let steps = 
            List.init cnt (fun _ -> 0)
            |> PSeq.map (fun _ -> try_one i x)
            |> PSeq.min
        steps >= (float i) + 2.0
        
    let try_one_stat i x =
        let cnt = 10
        let times = List.init cnt (fun _ -> try_one i x)
        let mean = List.sum times / (float cnt)
        let sd = sqrt  (List.sum (List.map (fun t -> (t - mean)**2.0) times)) / (float cnt)
        let min = List.min times
        let max = List.max times
        printf "i=%A avg=%A +-%A %A..%A\n" i mean sd min max
        min >= (float i) + 2.0
        
    let x = ref (Array.create hlen 0uy)
    let test_atpos (i:int) b =
        if i < Array.length known then 
            (!x).[i] <- known.[i]
            b = (known.[i])
        else
            let y = !x
            y.[i] <- b

            try_one_redundant i !x
            
    backtrack_find test_atpos 0 |> printf "success %b"

    x


// In[14]:

let handle1 (f:string, s:string) = 
    verify_request (String_bytes f) (s |> hex_decode |> Array.ofSeq) 

(*
let handle2 (f:string, s:string) =
    if verify_request (String_bytes f) (s |> hex_decode |> Array.ofSeq) then OK "gut"
    else ServerErrors.INTERNAL_ERROR "sig mismatch"

let app =
  choose [
    pathScan "/file/%s/%s" handle2
  ]

startWebServer defaultConfig app

let run_server = async {
        
}
let async_run_server () = Async.StartAsTask run_server

*)

// In[15]:

//async_run_server ()


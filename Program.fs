// Learn more about F# at http://fsharp.net
module Program

(*
#r packages/FSharp.Collections.ParallelSeq.1.0.2/lib/net40/FSharp.Collections.ParallelSeq.dll
#r packages/FsCheck.0.9.4.0/lib/net40-Client/FsCheck.dll 
#r packages/ExtCore.0.8.45/lib/net40/ExtCore.dll
*)

open System
open System.IO
open System.Security.Cryptography
open System.Collections.Generic
open System.Threading

open FSharp.Collections.ParallelSeq
open DataStruct



let DATA_DIR = @"../../data"
let hex_chars = "0123456789ABCDEF"
let base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
let english_freq = 
    let data = "A 	8.167%
        B 	1.492%
        C 	2.782%
        D 	4.253%
        E 	12.702%
        F 	2.228%
        G 	2.015%
        H 	6.094%
        I 	6.966%
        J 	0.153%
        K 	0.772%
        L 	4.025%
        M 	2.406%
        N 	6.749%
        O 	7.507%
        P 	1.929%
        Q 	0.095%
        R 	5.987%
        S 	6.327%
        T 	9.056%
        U 	2.758%
        V 	0.978%
        W 	2.360%
        X 	0.150%
        Y 	1.974%
        Z 	0.074%"
    let row_fn (row:string) = 
        let result = row.Trim(List.toArray [' '; '\n'; '%'; '\r']).Split('\t')
        (byte (result.[0].ToLower().[0]), float result.[1])

    dict (Seq.map row_fn (data.Split('\n')))

///  --- debugging --- 
let seqdebug (label:string) (x:seq<'a>) =
    x
    |> Seq.map (sprintf "%A")
    |> String.concat "; "
    |> printf "%A: %A\n" label

let debug label x =
    printf "%A: %A\n" label x
    x

let thprint str =
    System.Diagnostics.Debug.WriteLine str
    


let byte_hex_decode (s:char[]) = hex_chars.IndexOf(s.[0]) * 16 + hex_chars.IndexOf(s.[1]) |> byte

let seq_chop s l = 
    s
    |> Seq.windowed l
    |> Seq.mapi (fun i j -> (i, j))
    |> Seq.filter (fun (i, j) -> (i % l = 0))
    |> Seq.map (fun (_, j) -> j)

let seq_chop2 l (s:seq<'a>) =
    seq {
        let arr = Array.zeroCreate l
        let ix = ref 0
        for el in s do
            let mix = !ix % l
            if (mix = 0) && (!ix <> 0) then
                yield Array.copy arr

            arr.[mix] <- el
            ix := (!ix + 1)

        // the last array
        let left = if (!ix % l) <> 0 then (!ix % l) else (min l !ix)
        if left > 0 then
            let out = Array.sub arr 0 left
            yield out
    }

let extract_block blocksize block (inp: 'T[]): 'T[] =
    Array.sub inp (blocksize * block) blocksize

let tst = "abcdefg"
assert (Seq.forall (id) (Seq.map2 (=) (Seq.concat (seq_chop2 3 tst)) tst))

let hex_decode (s:string) =
    seq_chop (s.ToUpper()) 2
    |> Seq.map byte_hex_decode

let hex_encode d =
    let byte_hex_encode (b:byte) =
        let order = b / 16uy
        let num = b % 16uy
        [hex_chars.[int order]; hex_chars.[int num]]
    Seq.map byte_hex_encode d
    |> Seq.concat
    |> fun x -> String(Seq.toArray x)

let triplet_base64_encode d = 
    let is:int[] = Seq.toArray (Seq.map (fun x -> int x) d)
    in (is.[0] <<< 16) ||| (is.[1] <<< 8) ||| (is.[2])
        |> fun x -> [x >>> 18; x >>> 12; x >>> 6; x]
        |> Seq.map (fun x -> base64_chars.[x &&& 63])

let base64_encode d =
    Seq.append d (Seq.init ((Seq.length d) % 3) (fun _ -> 0uy))
    |> fun x -> seq_chop x 3
    |> Seq.map triplet_base64_encode
    |> Seq.concat
    |> Seq.toArray
    |> fun x -> String(x)

let base64_decode txt = Convert.FromBase64String txt

let hex_xor a b:string =
    Seq.map2 (fun x y -> byte (x ^^^ y)) (hex_decode a) (hex_decode b)
    |> hex_encode

let repeat_xor (text:byte[]) (key:byte[]) =
    Seq.mapi (fun i e -> e ^^^ (key.[i % key.Length])) text

let byte_lower (b:byte) =
    if (b >= (byte 'A')) && (b <= (byte 'Z'))
    then b - ((byte 'A') - (byte 'a'))
    else b

let bytes_String (x:'a) =
    String(x |> Seq.map char |> Seq.toArray)

let String_bytes x =
    Seq.map byte x |> Seq.toArray

let freq_analysis (txt:byte[]) =
    Seq.countBy id (Seq.map byte_lower txt)
    |> Seq.map (fun (c, f) -> (c, 100.0 * float f / float txt.Length))
    |> dict

let freq_analysis_rating txt =
    let result = freq_analysis txt
    let cmp_item (item:Collections.Generic.KeyValuePair<byte, float>) =
        if english_freq.ContainsKey(item.Key)
        then Math.Abs (english_freq.[item.Key] - item.Value)
        else 15.0
    Seq.map cmp_item result
    |> Seq.average


let singlechar_xor_keys = Seq.map (fun x -> Array.create 1 x) [0uy..255uy]
let xor_decipher k input_raw = repeat_xor (Seq.toArray input_raw) k
let best_singlechar_xor_decrypt input_raw =
    let item_fn k =
        let deciphered = xor_decipher k input_raw
        let rating = freq_analysis_rating (Seq.toArray deciphered)
        (k, rating, deciphered)
    Seq.map item_fn singlechar_xor_keys
    |> Seq.minBy (fun (_, rating, _) -> rating)

let print_decipher (key, rating, plaintext) =
    printf "%A, %f, %A" key rating (bytes_String plaintext)
    (key, rating, plaintext)

let hamming_bits (a:seq<byte>) (b:seq<byte>):int =
    let per_byte (bt:byte) = Seq.fold (fun cnt rsh -> cnt + int ((bt >>> rsh) &&& 1uy)) 0 [0..7]
    Seq.map2 (^^^) a b
    |> Seq.sumBy per_byte

assert ((hamming_bits (String_bytes "this is a test") (String_bytes "wokka wokka!!!")) = 37)

let pkcs7_pad len dat =
    if (Array.length dat) = len then
        dat
    else
        let p = byte (len - Array.length dat)
        Array.append dat (Array.create (int p) p)

let pkcs7_pad_seq len (dats:seq<byte[]>) =
    let dats_lst = List.ofSeq dats // need to remember anyway...
    let ll = (List.length dats_lst) - 1
    seq {
        for ix, el in Seq.mapi (fun i e -> (i, e)) dats_lst do
            yield pkcs7_pad len el
            if ((Array.length el) = len) && (ix = ll) then
                yield Array.create len (byte len)
    }

type InvalidPaddingException() =
    inherit Exception()

let cut_pkcs7_pad (bl:byte[]) =
    if Array.isEmpty bl then bl
    else 
        let p = bl.[bl.Length - 1]
        let correct = Seq.forall2 (fun ix el -> if ix >= bl.Length - (int p) then (el = p) else true) [0..bl.Length] bl
        if correct then
            Array.sub bl 0 (bl.Length - (int p))
        else
            raise (InvalidPaddingException())

let aes_ecb decrypt (key:byte[]) (dat:byte[]) =
    let aes = new AesManaged()
    let res:byte[] = Array.create 16 0uy
    aes.Mode <- CipherMode.ECB
    aes.Key <- key
    aes.BlockSize <- 8 * 16
    aes.Padding <- PaddingMode.Zeros
    let transform = if decrypt then aes.CreateDecryptor() else aes.CreateEncryptor()
    transform.TransformFinalBlock(dat, 0, 16) // |> debug "crypt"

// figure out how to pass the blocks out (seq {} ?)
// not really sure if this is correct
let cbc_encrypt algo (key:byte[]) (iv:byte[]) (blocks:seq<byte[]>) =
    let do_block pl iv =
        Array.map2 (^^^) iv pl
        |> algo false key

    seq {
        let prev = ref iv
        for pl in pkcs7_pad_seq 16 blocks do
            let cr = do_block pl !prev
            yield cr
            prev := cr
    }

let cbc_decrypt algo (key:byte[]) (iv:byte[]) (blocks:seq<byte[]>) =
    let do_block ci iv =
        algo true key ci
        |> Array.map2 (^^^) iv

    seq {
        let prev = ref iv
        for ci in blocks do
            yield (do_block ci !prev)
            prev := ci
    }

let remove_pkcs7_padding (stream:seq<byte[]>) =
    seq {
        let prev = ref [||]
        for bl in stream do
            if prev.Value.Length > 0 then
                yield !prev
            prev := bl

        // cut padding on prev and return it
        yield cut_pkcs7_pad (!prev)
    }

let cbc_encrypt_onepiece (key:byte[]) (iv:byte[]) (pl:byte[]) =
    seq_chop2 16 pl
    |> cbc_encrypt aes_ecb key iv
    |> Array.concat

let rand_bytes len =
    let result = Array.create len 0uy
    let r = new Random()
    r.NextBytes(result)
    result    

let test_cbc n =
    let pl = rand_bytes n
    let k = "yellow submarine" |> String_bytes
    let iv = "0123456789012345" |> String_bytes
    let pl_seq = seq_chop2 16  pl
    let cr = cbc_encrypt aes_ecb k iv pl_seq |> List.ofSeq
    let cr1 = Array.concat cr
    let decr = cbc_decrypt aes_ecb k iv cr |> remove_pkcs7_padding |> Seq.concat |> Array.ofSeq
    assert (pl = decr)

let ecb_crypt_onepiece decrypt key pl =
    let padding bl = if decrypt
                        then bl
                        else pkcs7_pad 16 bl
    pl
    |> seq_chop2 16
    |> Seq.map (padding >> aes_ecb decrypt key)

let detect_ecb encryption_oracle =
    let tested = encryption_oracle (Seq.init 2048 (fun _ -> 0uy)) |> seq_chop2 16
    let ar4 = Seq.take 4 tested |> Seq.toArray
    let a, b = ar4.[2], ar4.[3]
    Seq.forall2 (=) a b

let keyval_encode (dct:Map<string, string>) =
    String.concat "&"  (Seq.map (fun (k, v) -> k + "=" + v) (Map.toSeq dct))

let keyval_encode_profile (dct:Map<string, string>) =
    let items = ["email"; "uid"; "role"]
    String.concat "&" (Seq.map (fun x -> x + "=" + dct.[x]) items)

let keyval_decode (str:string) =
    let kv_fn (map:Map<string, string>) (item:string) =
        try
            let parts = item.Split('=')
            Map.add parts.[0] parts.[1] map
        with
            _ -> printf "keyval decoding error"
                 map
    
    Seq.fold kv_fn Map.empty (str.Split('&'))

let Option_seq_first s =
    Option.attempt (fun () -> Seq.find Option.isSome s |> Option.get)


let rec sequential_decode 
            check_byte_fn 
            (already_known: byte[]) 
            (len: int)
            (append_fn: byte[] -> byte -> byte[]) =
    match len with
    | 0 -> Some already_known
    | _ -> let b_options = Seq.filter (check_byte_fn already_known) [0uy..255uy]
           let recurse b = 
               sequential_decode check_byte_fn (append_fn already_known b) (len - 1) append_fn
           let res = Seq.map recurse b_options
           Option_seq_first res

let ECB_oracle_decode check_byte_fn already_known len =
    sequential_decode check_byte_fn already_known len (fun a b -> 
        printf "%s" (b |> char |> string)
        Array.append a [| b |])
    |> Option.get

let rec CTR_run (algo: bool -> byte[] -> byte[] -> byte[]) key (nonce:uint64) (ix:uint64) =
    let blocksize = 16
    let ix_bytes = BitConverter.GetBytes(ix)
    let nonce_bytes = BitConverter.GetBytes(nonce)
    //let ix_padded = Array.concat [(Array.zeroCreate (blocksize - ix_bytes.Length)); ix_bytes]
    seq {
        yield (algo false key (Array.append nonce_bytes ix_bytes))
        yield! CTR_run algo key nonce (ix + 1UL)
    }
    
let xor_blocks (a: byte[]) (b: byte[]) =
    let ln = min (Array.length a) (Array.length b)
    Array.map2 (^^^) (Array.sub a 0 ln) (Array.sub b 0 ln)

let CTR_crypt algo key nonce cr =
    Seq.map2 xor_blocks cr (CTR_run algo key nonce 0UL)

let CTR_crypt_onepiece key nonce cr =
    seq_chop2 16 cr
    |> CTR_crypt aes_ecb key 0UL
    |> Seq.concat
    |> Array.ofSeq

let rec MT_run (mt:MersenneTwister) =
    let n = mt.ExtractNumber()
    seq {
        yield n &&& 0xffu |> byte
        yield (n >>> 8) &&& 0xffu |> byte
        yield (n >>> 16) &&& 0xffu |> byte
        yield (n >>> 24) &&& 0xffu |> byte
        yield! MT_run mt
    }

let MT_stream_crypt_onepiece (seed:uint16) txt =
    Seq.map2 (^^^) (MT_run (new MersenneTwister(uint32 seed))) txt

module AuthBitflip =
    let cook_start = "comment1=quick%20buck;userdata="
    let make_cookie (inp:String) = 
        let quoted_inp = inp.Replace(";", "\;").Replace("=", "\=")
        String.concat "" [ 
            cook_start;
            quoted_inp;
            ";comment2=is%20hard" ]
    let flip_last_bit (arr: byte[]) pos =
        let res = Array.copy arr
        res.[pos] <- arr.[pos] ^^^ 1uy
        res

type InvalidCryptoInputException(str) =
    inherit Exception()

    member this.str:String = str

let sha256 (inp:byte[]):byte[] =
    let s = new SHA256Managed()
    s.ComputeHash(inp)


let set1 () =
    let task1 =
        let input = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
        let output = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
        //printf "%A" (input |> hex_decode |> base64_encode)
        assert ((input |> hex_decode |> base64_encode) = output)

    let task2 =
        let input1 = "1c0111001f010100061a024b53535009181c"
        let input2 = "686974207468652062756c6c277320657965"
        let output = "746865206b696420646f6e277420706c6179"
        assert ((hex_xor input1 input2).ToLower() = output.ToLower())

    let task3 =
        let input = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
        let input_raw = hex_decode input
        best_singlechar_xor_decrypt input_raw

    let task4 () =
        let input = File.ReadAllLines(Path.Combine(DATA_DIR, "4.txt"))
        let input_raw = Seq.map hex_decode input
        let best = Seq.minBy (fun (_, rating, _) -> rating) (Seq.map best_singlechar_xor_decrypt input_raw)
        print_decipher best

    let task5 =
        let input = ["Burning 'em, if you ain't quick and nimble"; "\nI go crazy when I hear a cymbal"]
        let output = ["0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20"; "430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f"]
        let keys = ["ICE"; "ICE"]
        let item_fn ix _ =
            let i = input.[ix]
            let o = output.[ix]
            let k = keys.[ix]
            let result = (xor_decipher (String_bytes k) (String_bytes i) |> hex_encode).ToLower()
            assert (result = o)
        Seq.iteri item_fn input

    let task6 () =
        let input = File.ReadAllLines(Path.Combine(DATA_DIR, "6.txt"))
        let input_raw = base64_decode (String.Join("", input))
        let rate_keysize keysize =
            let chops = Seq.map (fun ix -> Array.sub input_raw (ix*keysize) keysize) [0..4]
            Seq.map (fun a -> Seq.map (fun b -> float (hamming_bits a b) / (float keysize)) chops) chops
            |> Seq.concat
            |> Seq.average
        let keysize_estimates = Seq.take 1 (Seq.sortBy rate_keysize [2..40] |> debug "keysize_estimates") |> Seq.toArray
        let try_something keysize =
            // take the blocks of input of keysize length
            // transpose them (put them above each other)
            // work with individual columns ((keysize * i) + colnum)
            let col_size colnum = (input_raw.Length / keysize) + (if (input_raw.Length % keysize) > colnum then 1 else 0)
            let mk_col colnum = Seq.init (col_size colnum) (fun ix -> input_raw.[(ix * keysize) + colnum])
            let col_with_key (k, rating, deciphered) = 5
            let colkeys = Seq.map (mk_col >> best_singlechar_xor_decrypt >> (fun (a, b, c) -> (a.[0], b, Seq.toArray c))) [0..keysize-1] |> Seq.toArray
            let decipher_generator ix =
                let ar_colkeys = Seq.toArray colkeys
                match ar_colkeys.[ix % keysize] with
                | (a, b, c) -> c.[ix / keysize]
            
            Seq.map (fun (k, _, _) -> k) colkeys
            |> bytes_String
            |> printf "%A\n"

            Seq.init input_raw.Length decipher_generator
            |> bytes_String
            |> printf "decoded: \n %A\n"
        
        Seq.iter try_something keysize_estimates

    let task7 () =
        let input = File.ReadAllLines(Path.Combine(DATA_DIR, "7.txt"))
        let input_raw = base64_decode (String.Join("", input))
        let key = String_bytes "YELLOW SUBMARINE"
        let aes = new AesManaged()
        aes.Mode <- CipherMode.ECB
        aes.Key <- key
        aes.BlockSize <- 8 * 16
        
        use out_stream = new MemoryStream()
        use crypto_stream = new CryptoStream(out_stream, aes.CreateDecryptor(), CryptoStreamMode.Write)
        use writer = new BinaryWriter(crypto_stream)
        writer.Write input_raw
        out_stream.ToArray()
        |> bytes_String
        |> debug "output"

    let task8 () =
        let inputs = File.ReadAllLines(Path.Combine(DATA_DIR, "8.txt"))
        let inputs_raw = Seq.map hex_decode inputs
        let rec seq_split cnt seq =
            match cnt, seq with
            | 0, x :: xs -> ([], x :: xs)
            | c, [] -> ([], [])
            | c, x :: xs -> let (ra, rb) = seq_split (c - 1) xs
                            (x :: ra, rb)
        let detect_repetition ix line =
            let (tbl) = new Dictionary<int, byte array list>()
            let rec eat_line ln =
                let ra, rb = seq_split 16 ln |> fun (x, y) -> (Seq.toArray x, y)
                let int_sum (a:int) b = (int b) + a
                let key = Seq.fold int_sum 0 ra
                let slow_array_eq = Array.forall2 (=)
                if tbl.ContainsKey key then
                    let candidates = tbl.[key]
                    if not (List.forall (fun cand -> not (slow_array_eq cand ra)) candidates) then
                        printf "Got A repetition in line %d\n" ix
                else
                    tbl.Add(key, [ra])
                    if rb <> [] then eat_line rb
            eat_line (Seq.toList line)

        // let's detect repetition
        Seq.iteri detect_repetition inputs_raw
    
    let task9 =
        pkcs7_pad 16 [|1uy; 1uy; 4uy; 4uy|]

    let task10 () =
        let input_raw = base64_decode (File.ReadAllText(Path.Combine(DATA_DIR, "10.txt")))
        let cblock = 16
        let mine = cbc_decrypt aes_ecb
                    (String_bytes "YELLOW SUBMARINE")
                    (Array.create cblock 0uy)
                    (seq {for i in 0..(input_raw.Length / cblock)-1 do yield Array.sub input_raw (i*cblock) cblock })

        mine
        |> remove_pkcs7_padding
        |> Seq.concat
        |> Seq.toArray
        |> bytes_String
        |> debug "result"
        
    let task11 () =
        let encryption_oracle (pl:seq<byte[]>) =
            let k = rand_bytes 16
            let iv = rand_bytes 16
            let r = new Random()
            let bl1 = r.Next(5, 10)
            let bl2 = r.Next(5, 10)
            let amended_stream = Seq.append (Seq.append (Seq.singleton (Array.create bl1 65uy)) pl) (Seq.singleton (Array.create bl2 66uy))
                                |> Array.concat
                                |> seq_chop2 16
            let ecb = (r.Next(0, 2) = 1)
            let ecb_crypt algo decrypt key dat =
                Seq.map (pkcs7_pad 16 >> algo decrypt key) dat
            
            debug "Using ECB: " ecb |> ignore
            if ecb then
                ecb_crypt aes_ecb false k amended_stream
            else
                cbc_encrypt aes_ecb k iv amended_stream

        detect_ecb (seq_chop2 16 >> encryption_oracle >> Seq.concat)
        |> debug "guessed ECB: "

    let task12 () =
        let unknown_string = 
            "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK"
            |> base64_decode
        let key = [|1; 3; 5; 7; 11; 13; 17; 19; 23; 27; 29; 31; 33; 35; 37; 39 |] |> Array.map byte
        let encrypt_oracle plaintext =
            let final_plaintext = Array.append plaintext unknown_string
            ecb_crypt_onepiece false key final_plaintext
            |> Array.concat
        let aa_generator = Seq.initInfinite (fun x -> Array.create x (byte 'A'))
        let guess_blocksize =
            let sizes = [for x in Seq.take 33 aa_generator -> encrypt_oracle x |> Array.length]
            let s1 = sizes.Head
            (List.find (fun x -> x <> s1) (List.tail sizes)) - s1
            |> debug "blocksize:"
        assert (detect_ecb (Seq.toArray >> encrypt_oracle))
        
        (* 
        secret text: ABCDEFGHIJKLMN...
        fill: .
        tested char: X
        blocksize: 4

        ABCD EFGH IJKL
        ABCD 

        ...X|BCDE
        ..AX|CDEF
        .ABX|DEFG
        ABCX|EFGH
        
        .... ...X
        I need to guess E:
        ...A BCDE FGHI
        compares with
        ...A BCDx FGHI
        ..AB CDEF GHIJ

        *)
        let check_byte already_known b =
            let bs = guess_blocksize
            let al = Array.length already_known
            let block = al / bs
            let suffix = Seq.append already_known [| b |]
            let prefix = Array.create (bs - (al % bs) - 1) (byte 'A')
            let cr = encrypt_oracle (Array.append prefix (Seq.toArray suffix))
            let cr_short = encrypt_oracle prefix
            Seq.forall2 (=) (Array.sub cr (bs * block) bs) (Array.sub cr_short (bs * block) bs)
        


        let decode = ECB_oracle_decode check_byte
        (decode [||] (unknown_string.Length - 1))

    let task13 () =
        let profile_for (email:string) =
            let stripped = email.Replace('&', '_').Replace('=', '_')

            Map.ofList [("email", email); ("uid", "10"); ("role", "user")] |> keyval_encode_profile
        let unknown_key = String_bytes "zealous thunderx"
        let blocksize = 16
        let encrypt_cookie email = ecb_crypt_onepiece false unknown_key (profile_for email |> String_bytes) |> Array.concat
        let decrypt_cookie cr = ecb_crypt_onepiece true unknown_key cr |> Array.concat
        let role pl = (keyval_decode (bytes_String pl)).["role"]

        let e1 = "a@b.12345cadmin"
        let e2 = "achhkuh@uaoco"
        let c1 = encrypt_cookie e1
        let c2 = encrypt_cookie e2
        let part1 = c1.[blocksize*1.. (blocksize*2-1)]
        let part2 = c2.[0..(blocksize*2 - 1)]

        let forged_raw = Seq.append part2 part1 |> Seq.map byte 
        let forged = forged_raw |> bytes_String

        assert ((forged |> String_bytes |> decrypt_cookie |> role) = "admin")
        1

    let task14 () =
        let prefix = [|12uy; 87uy; 83uy; 83uy; 27uy; 09uy; 123uy; 90uy; 43uy; 239uy; 38uy; 11uy; 33uy; 11uy; 17uy; 88uy; 33uy|]
        let postfix = "Hello Kitty Rilakkuma LINE friends" |> String_bytes
        let blocksize = 128 / 8
        let key = [|1; 3; 5; 7; 11; 13; 17; 19; 23; 27; 29; 31; 33; 35; 37; 39 |] |> Array.map byte
        let encrypt_oracle plaintext =
            let final_plaintext = Array.concat [ prefix; plaintext; postfix ]
            ecb_crypt_onepiece false key final_plaintext |> Array.concat

        let changed_block abl bbl =
            // assuming the chosen plaintext part is same length,
            // return the index of block that differs between the two plaintexts
            (Seq.zip abl bbl |> Seq.findIndex (fun (x, y) -> x <> y)) / 16

        let compare_blocks abl bbl: bool list =
            (Seq.zip (seq_chop2 blocksize abl) (seq_chop2 blocksize bbl) |> Seq.map (fun (x, y) -> x = y) |> List.ofSeq)

        let cr_empty = encrypt_oracle [||]
        let cr_oneA = encrypt_oracle [| byte 'A' |]

        let fn_chng_block cnt =
            // does cnt * 'A' + B detect the border of a block in chosen plaintext block?
            let pl_base = Array.create cnt (byte 'A')

            changed_block 
                (encrypt_oracle (Array.append pl_base [| byte 'A' |]))
                (encrypt_oracle (Array.append pl_base [| byte 'B' |]))

        let changes_list = Seq.map fn_chng_block (Seq.init (blocksize * 8) id) |> Seq.toList
        let bytes_to_end = Seq.findIndex (fun x -> x <> (List.head changes_list)) changes_list
        
        let check_byte already_known b =
            let bs = blocksize
            let al = Array.length already_known
            let bte = bytes_to_end
            let block = Seq.head changes_list + (al / bs) + 1
            let suffix = Array.append already_known [| b |]
            let prefix_len = (bte + bs - (al % bs))
            let prefix = Array.create (prefix_len - 1) (byte 'A')
            let cr = encrypt_oracle (Array.append prefix suffix)
            let cr_short = encrypt_oracle prefix
            let extr_block inp = extract_block blocksize block inp
            Seq.forall2 (=) (extr_block cr) (extr_block cr_short)
            
        ECB_oracle_decode check_byte [||] (postfix.Length - 1)

    let task15 () =
        let correct1 = "ICE ICE BABY\x04\x04\x04\x04"
        let incorrect1 = "ICE ICE BABY\x05\x05\x05\x05"
        let incorrect2 = "ICE ICE BABY\x01\x02\x03\x04"

        let try_ok inp exp =
            assert ((cut_pkcs7_pad (String_bytes inp)) = (String_bytes exp))
        
        let try_fail inp =
            try
                cut_pkcs7_pad (String_bytes inp) |> ignore
                assert false
            with :? InvalidPaddingException ->
                assert true

        try_ok correct1 "ICE ICE BABY"
        try_fail incorrect1
        try_fail incorrect2

    let task16 () =
        let blocksize = 16
        let key = rand_bytes 16
        let iv = rand_bytes blocksize
        let encr_cookie inp = 
            cbc_encrypt aes_ecb key iv (AuthBitflip.make_cookie inp |> String_bytes |> seq_chop2 blocksize)
            |> Seq.concat
            |> Array.ofSeq
        let isadmin cr_cook = 
            cbc_decrypt aes_ecb key iv (seq_chop2 blocksize cr_cook)
            |> Seq.concat
            |> bytes_String |> debug "decrypted"
            |> (fun s -> s.Contains(";admin=true;"))
           
        let to_flip = [blocksize + 0; blocksize + 6]

        // we want to make : into ; by flipping the last bit
        let starting = new String('A', blocksize - (AuthBitflip.cook_start.Length % blocksize)) + ":admin<true"
        
        let encr = encr_cookie starting
        let encr_mod = Seq.fold AuthBitflip.flip_last_bit encr to_flip

        assert (isadmin encr_mod)

    let task17 () =
        let key =  rand_bytes 16
        let blocksize = 16
        let datas = "MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc=
MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic=
MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw==
MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg==
MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl
MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA==
MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw==
MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8=
MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g=
MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93".Split([| '\n' |])
        let all_encrypt = 
            //let pl = datas.[(new Random()).Next(datas.Length)]
            let pl = datas.[3] |> base64_decode
            debug "plaintext" (bytes_String pl) |> ignore
            let iv = rand_bytes blocksize
            ((cbc_encrypt aes_ecb key iv (seq_chop2 blocksize pl)) |> List.ofSeq, iv)
        let all_decrypt cr iv =
            try
                cbc_decrypt aes_ecb key iv cr
                |> remove_pkcs7_padding
                |> Seq.iter ignore
                true
            with :? InvalidPaddingException -> false

        let cr_all, iv_all = all_encrypt
        let cr = iv_all :: cr_all
        let decode_block b_ix = 
            let try_byte (already_known:array<byte>) b =
                let al = Array.length already_known
                let pad = byte (al + 1)
                let cr0 = cr.[b_ix]
                let cr0t = 
                    let build_fn i e =
                        if      (i + al + 1 < blocksize) then e
                        else if (i + al + 1 = blocksize) then b
                        else    (already_known.[i - (blocksize - al)] ^^^ pad ^^^ cr0.[i])
                    Array.mapi build_fn cr0
                let cr1 = cr.[b_ix + 1]
                let res = all_decrypt [cr0t; cr1] [| for _ in [1..blocksize] -> 0uy |]
                res

            let append_fn a b =
                let al = Array.length a
                let pad = byte (al + 1)
                let p = cr.[b_ix].[blocksize - al - 1] ^^^ pad ^^^ b
                let res = Array.append [| p |] a
                printf "\r%s" (bytes_String res)
                res

            // there could be more than 1 match
            sequential_decode try_byte [||] (blocksize) append_fn
        
        List.iteri (fun i e -> 
            decode_block i |> Option.get |>  bytes_String |> debug "result" |> ignore
            printf "\n"
            ) cr_all

    let task18 () =
        let cr = "L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ==" |> base64_decode
        let key = "YELLOW SUBMARINE" |> String_bytes
        seq_chop2 16 cr
        |> CTR_crypt aes_ecb key (0UL)
        |> Seq.concat
        |> bytes_String
        |> debug "result"
        
    let task19 () =
        let key = "yellow SUBMARINE" |> String_bytes
        let datas = 
            File.ReadAllLines(Path.Combine(DATA_DIR, "20.txt"))
            |> Seq.map base64_decode
            |> List.ofSeq
        let decrypt_col ix b =
            let decrypt_col_in ix b (cr:byte[]) =
                cr.[ix] ^^^ b
            List.map (decrypt_col_in ix b) datas
        let guess_col ix =
            let rate_option b =
                let column = decrypt_col 0 b |> Array.ofList
                let rating = freq_analysis_rating column
                (b, Array.map char column, rating)
            let options = 
                List.map rate_option [0uy..255uy]
                |> List.sortBy (fun (b, c, r) -> r)
            match options.[0] with
            | (b, c, r) -> b
        let len = 
            datas
            |> List.map Array.length
            |> List.max
        let keystream = List.map guess_col [ 0..len ]
        List.iter (fun cr -> 
            cr
            |> Seq.map2 (^^^) keystream
            |> bytes_String
            |> printf "%s\n"
        ) datas
            
    let task21 () =
        let mt = new MersenneTwister(1u)
        printf "%d, " (mt.ExtractNumber())
        printf "%d" (mt.ExtractNumber())
        // 1791095845, 4282876139
    let task22 () =
        let mil1 = (1L*1000L*1000L)  // used to convert into seconds or 1/10ths of seconds
        let gen_target () =
            let r = new Random()
            Thread.Sleep(r.Next(40, 200) * 1000)
            let now = DateTime.Now.Ticks / mil1
            let target = (new MersenneTwister(uint32(now))).ExtractNumber()
            Thread.Sleep(r.Next(40, 100) * 1000)
            printf "target is %x\n" target
            target
        
        let breakit tgt =
            let max = DateTime.Now.Ticks / mil1
            let min = max - 3000L
            let test_fn n =
                let mt = new MersenneTwister(uint32(n))
                let num = mt.ExtractNumber()
                if (num = tgt) then printf "num is %x with seed %x\n" num n
                num = tgt
            Seq.find test_fn (Seq.init ((max - min) |> int) ((+) (int(min))))
        
        printf "seed is %x" (gen_target() |> breakit)

    let task23 () =
        let orig = new MersenneTwister(DateTime.Now.Ticks / 1000L / 100L |> uint32)
        let tap = [| for _ in [0..623] -> orig.ExtractNumber() |]
        let state = Array.map orig.Untemper tap
        let copy = new MersenneTwister(0u)
        copy.SetState(state)
        for _ in [0..15] do
            assert (copy.ExtractNumber() = orig.ExtractNumber())

        // hashing the output of MT wouldnt help because the input and output
        // space is only 32 bits so its possible to build a dictionary
        // to thwart reversing, it may help to introduce non-reversible changes
        // so the attacker would have to start guessing, of course they have to
        // build up to 128 bits over the state of 624 ints

    let task24 () =
        let r = new Random()
        let test () =
            let test_txt = rand_bytes (r.Next(32, 512))
            let seed = uint16 (r.Next(1, 65535))
            let test_cr = MT_stream_crypt_onepiece seed test_txt
            assert (test_txt = Array.ofSeq (MT_stream_crypt_onepiece seed test_cr))
        test()

        let prefix = rand_bytes (r.Next(6, 32))
        let suffix = Array.create 14 (byte 'A')
        let seed = r.Next(1, int UInt16.MaxValue) |> uint16
        let inp = (Array.append prefix suffix)
        let cr = MT_stream_crypt_onepiece seed inp |> Array.ofSeq
        // at first I thought, ok, what cool XOR trick should be used to 
        // recover the seed? But since it's 16b, why not spend 0.7s of my CPU
        // time to brute force it??
        let cmp x y = 
            //(Seq.compareWith (fun x y -> if (x = y) then 0 else 1) x y) = 0
            Seq.map2 (=) x y
            |> Seq.exists not  // any of items is false?
            |> not             // if any is false, return true

        let test1 () =
            let test_seed_fn x =
                MT_stream_crypt_onepiece x inp
                |> cmp cr
            let candidates = [ UInt16.MinValue .. UInt16.MaxValue ]
            let seed_fnd = PSeq.find test_seed_fn candidates
            printf "was %A found: %A" seed seed_fnd
        
        let pwd_things() =
            let take32b n = uint32(n &&& (int64 UInt32.MaxValue))
            let seed = DateTime.Now.Ticks
            let mt = new MersenneTwister(take32b seed)
            let token = Seq.take 16 (MT_run mt) |> Array.ofSeq  // code reuse!
            let delay = 15 * 1000 * 1000  // up to 15s (10mil per sec)
            let start = DateTime.Now.Ticks

            //let fnd = PSeq.find (test_token_fn token) (PSeq.init delay (fun i -> start - (uint32 i)))
            let workers = 4u
            let cancel_src = new CancellationTokenSource()
            let computation_result = ref None
            let seg_size = uint32 delay / workers
            let run_worker ix =
                let state = Array.create 624 0u

                let test_token_fn seed =
                    Seq.take 16 (MT_run (new MersenneTwister(take32b seed, state)))
                    |> cmp token
                let start_time = DateTime.Now
                let high = start - int64(seg_size * ix)
                let low = start - int64(seg_size * (ix + 1u) - 1u)
                sprintf "Thread work %A..%A" low high |> thprint
                let nums = seq { low .. high }
                let measure_fn (x:int64) =
                    if ((x - low) = 1L * 1000L * 1000L) then
                        let stop_time = DateTime.Now
                        sprintf "1 mil in %A" (stop_time - start_time) |> thprint
                    test_token_fn x
                let found = Seq.tryFind measure_fn nums
                // this is incomplete and doest work :P
                match found with
                | Some x -> 
                        computation_result.Value = found |> ignore
                        cancel_src.Cancel()
                        found
                | _ -> None

            let fnd = 
                try
                    Async.RunSynchronously(
                        Async.Parallel [ for i in 0u..(workers-1u) -> async { return run_worker (i) } ],
                        -1, cancel_src.Token) |> ignore
                    None
                with
                :? OperationCanceledException -> computation_result.Value
                
            printf "was %A found %A " seed fnd
        
        pwd_things ()


    let task25 () =
        let input = seq_chop2 16 (String_bytes ("At the time of this writing, its possible to run F# code through Visual Studio, through its interactive top-level F# Interactive (fsi), and compiling from the command line. This book will assume that users will compile code through Visual Studio or F# Interactive by default, unless specifically directed to compile from the command line.
                Setup Procedure
                F# can integrate with existing installations of Visual Studio 2008 and is included with Visual Studio 2010. Alternatively, users can download Visual Studio Shell for free, which will provide an F# pioneer with everything she needs to get started, including interactive debugging, breakpoints, watches, Intellisense, and support for F# projects. Make sure all instances of Visual Studio and Visual Studio Shell are closed before continuing.
                To get started, users should download and install the latest version of the .NET Framework from Microsoft. Afterwards, download the latest version of F# from the F# homepage on Microsoft Research, then execute the installation wizard. Users should also consider downloading and installing the F# PowerPack, which contains handy extensions to the F# core library."))

        let key = rand_bytes 16
        let crypto = CTR_crypt aes_ecb key 0UL input |> List.ofSeq

        let edit cr k off (newtext:byte array) = 
            let block = off / 16
            let bl_off = off % 16
            let keystream = 
                CTR_run aes_ecb k 0UL 0UL
                |> Seq.skip block
            let keystream_head = Seq.head keystream
            let keystream_mod = Seq.append [ keystream_head ] (Seq.skip 1 keystream)
            let pl_1block = 
                Seq.head cr
                |> Seq.map2 (^^^) keystream_head

            // nah, only support aligned edits, thats all we need
            assert (bl_off = 0)
            assert ((Array.length newtext) = 16)
            let reencrypted =
                Array.map2 (^^^) newtext keystream_head
            Seq.concat [ (Seq.take block cr); Seq.ofList [ reencrypted ]; (Seq.skip (block + 1) cr) ]

        let mk0 cr off:byte array = edit cr key off (Array.create 16 0uy) |> Seq.item (off / 16)
        let (recov_keystream:byte array seq) = Seq.init (List.length crypto) (fun ix -> mk0 crypto (16 * ix))
        let decrypt = Seq.map2 xor_blocks recov_keystream crypto
        printf "%s" (bytes_String (Seq.concat decrypt))
        assert (decrypt = input)

    let task26 () =
        let blocksize = 16
        let key = rand_bytes blocksize
        let make_cookie = AuthBitflip.make_cookie >> String_bytes
        let encrypt_cookie = seq_chop2 blocksize >> CTR_crypt aes_ecb key 0UL >> Array.concat
        let decrypt_cookie = seq_chop2 blocksize >> CTR_crypt aes_ecb key 0UL >> Array.concat
        let isadmin pl = (bytes_String pl).Contains(";admin=true;")

        let attack_dat = ":admin<true"
        let attack_cr =
            make_cookie attack_dat
            |> encrypt_cookie
        let mod_cookie cook =
            let positions = [ 31; 31 + 6 ]
            Seq.fold AuthBitflip.flip_last_bit cook positions
        let result () = 
            attack_cr
            |> mod_cookie
            |> decrypt_cookie
            |> (fun s -> 
                debug "mod_cookie pl" (bytes_String s) |> ignore
                s)
            |> isadmin

        result()

    let task27 () =
        let blocksize = 16
        let key = rand_bytes blocksize
        debug "orig key: " key |> ignore
        let orig_msg = "Applications sometimes use the key as an IV on the auspices that both the sender and the receiver have to know the key already, and can save some space by using it as both a key and an IV."


        let valid_ascii s = 
            if not (Seq.forall ((>) 128uy) (String_bytes s)) then raise (InvalidCryptoInputException (s))
        let crypto = 
            orig_msg
            |> String_bytes
            |> seq_chop2 blocksize
            |> cbc_encrypt aes_ecb key key 
            |> List.ofSeq
        let mod_crypt = [ crypto.[0]; Array.create blocksize 0uy; crypto.[0] ]

        let receive cr =
            cr
            |> cbc_decrypt aes_ecb key key
            |> Seq.concat
            |> bytes_String
            |> valid_ascii
        try
            receive mod_crypt
        with :? InvalidCryptoInputException as e -> 
            let pl_blocks = 
                e.str
                |> String_bytes
                |> seq_chop2 blocksize
                |> List.ofSeq
            let recover_key = Array.map2 (^^^) pl_blocks.[0] pl_blocks.[2]
            debug "recov key: " recover_key |> ignore
            assert (recover_key = key)

        
    in
        task27 ()
        
let all_selftests () =
    List.map test_cbc [0..129] |> ignore
    DataStruct.all_selftests ()

// do run?
if false then
    all_selftests () |> ignore

    let result = set1 ()
    printf "%A done" result

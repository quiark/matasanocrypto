module task33

open Program

open System
open System.Numerics
open System.Security.Cryptography
open System.Globalization

open FSharp.Collections.ParallelSeq


let sha1 (x:byte[]) =
    let s = new SHA1Managed()
    s.ComputeHash(x)

let modpow a b p = BigInteger.ModPow(a, b, p)

let hmac_sha256 k (v:byte[]) =
    let s = new HMACSHA256(k)
    s.ComputeHash(v)


let DH p g = 
    let rnd = new Random()
    let a = bigint(rnd.Next()) % p
    let b = bigint(rnd.Next()) % p
    let A = BigInteger.ModPow(g, a, p)
    let B = BigInteger.ModPow(g, b, p)

    let s1 = BigInteger.ModPow(B, a, p)
    let s2 = BigInteger.ModPow(A, b, p)

    if s1 <> s2 then failwith "eh"

    sha1(s1.ToByteArray())

let p = BigInteger.Parse("
0ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024
e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd
3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec
6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f
24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361
c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552
bb9ed529077096966d670c354e4abc9804f1746c08ca237327fff
fffffffffffff".Replace("\n", ""), NumberStyles.HexNumber)

let g = 2I

// Task 34, Implement a MITM key-fixing attack on Diffie-Hellman with parameter injection

let g_p = p
let g_g = g
let rnd = new Random()


let big_rand (modulo:bigint) =
    // not actually uniform distribution but here we don't care
    let buf = modulo.ToByteArray()
    rnd.NextBytes(buf)
    let res = BigInteger.Abs(BigInteger(buf)) % modulo
    res

type DHMessaging () = 
    let bigrandom () =
        big_rand p

    let mutable p = bigint 0
    let mutable g = bigint 0
    let mutable A = bigint 0
    let mutable B = bigint 0
    let mutable a = bigint 0
    let mutable b = bigint 0
    let mutable s = bigint 0
    
    // accessors
    member this.locals () =
        (A, B, p, g)
    (*
    member this.Ap
        with get() = A
        and set(v) = A <- v
    
    member this.Bp
        with get() = B
        and set(v) = B <- v
*)
    member this.setGroup (p_, g_) =
        p <- p_
        g <- g_

    member this.kdf (s:bigint) =
        Array.sub (sha1 (s.ToByteArray())) 0 16
        
    member this.decrypt (cr, iv) s =
        let key = this.kdf s
        cbc_decrypt aes_ecb key iv cr
        |> remove_pkcs7_padding

    member this.encrypt s msg =
        let key = this.kdf s
        let iv = rand_bytes 16
        let cr = cbc_encrypt aes_ecb key iv msg
        (cr, iv)


    member this.step1_a () =
        p <- g_p
        g <- g_g
        
        //, make, a, random, number, of, the, same, size)
        a <- bigrandom ()
        A <- BigInteger.ModPow(g, a, p)
        (p, g, A)
        
    abstract member step2_b : bigint * bigint * bigint -> bigint
    default this.step2_b (p_, g_, A_) =
        p <- p_
        g <- g_
        
        A <- A_
        b <- bigrandom()
        B <- BigInteger.ModPow(g, b, p)
        s <- BigInteger.ModPow(A, b, p)
        B
        
    member this.step3_a B_ =
        B <- B_
        s <- BigInteger.ModPow(B, a, p)
        
        let iv = rand_bytes 16
        let key = this.kdf s
        let msg =
            "if they eventually prove to be correct, they receive tokens – whereas if they prove to be incorrect, they incur electricity costs without compensation."
            |> String_bytes
            |> seq_chop2 16
        let cr = cbc_encrypt aes_ecb key iv msg
        (cr, iv)
        
    member this.step4_b (cr_, iv_) =
        let key = this.kdf s
        let pl = this.decrypt (cr_, iv_) s
        let iv2 = rand_bytes 16
        let cr2 = cbc_encrypt aes_ecb key iv2 pl
        (cr2, iv2)
        
    member this.step5_a (cr2_, iv2_) =
        this.decrypt (cr2_, iv2_) s

let basic_comm () =
    let sa = new DHMessaging()
    let sb = new DHMessaging()
    
    sa.step1_a ()
    |> sb.step2_b
    |> sa.step3_a
    |> sb.step4_b
    |> sa.step5_a
    |> Seq.concat
    |> bytes_String
    |> printf "%A"


type DHMitm () =
    inherit DHMessaging()
    
    let mutable p = bigint 0
    
    member this.step12 (p_, g_, A_) =
        p <- p_
        (p_, g_, p_)
        
    member this.step23 B_ =
        p
        
    member this.step34 (cr_, iv_) =
        let key = this.kdf (bigint 0)
        let pl = cbc_decrypt aes_ecb key iv_ cr_
        pl
        |> Seq.concat
        |> bytes_String
        |> printf "hahahahHAHAHAHAHAHAAAAA!\n %A\n"
        (cr_, iv_)
        
    member this.step45 (cr2_, iv2_) =
        (cr2_, iv2_)

let mitm_comm () =
    let sa = new DHMessaging()
    let sb = new DHMessaging()
    let sm = new DHMitm()

    sa.step1_a ()
    |> sm.step12
    |> sb.step2_b
    |> sm.step23
    |> sa.step3_a
    |> sm.step34
    |> sb.step4_b
    |> sm.step45
    |> sa.step5_a


type Response = ACK | ERR

type DHVarGroup () =
    inherit DHMessaging()

    member this.stepv1_a () =
        let p, g, A = this.step1_a ()
        (p, g)

    member this.stepv2_b (p_, g_) =
        this.setGroup(p_, g_)
        ACK

    member this.stepv3_a code =
        let A, B, a, b = this.locals()
        A

    member this.stepv4_b A =
        let _, _, p, g = this.locals()
        let B = this.step2_b (p, g, A)
        B

    member this.stepv5_a B =
        this.step3_a B

    member this.stepv6_b (cr_, iv_) =
        this.step4_b (cr_, iv_)

    member this.stepv7_a (cr_, iv_) =
        this.step5_a (cr_, iv_)


type DHVarGroupMitm () =
    inherit DHMessaging ()

    let mutable m_p = bigint 0
    let mutable m_g = bigint 0
    let mutable msg:byte array list = []
    let mutable A = bigint 0

    member this.Message
        with get() = msg

    abstract member stepv_12: bigint * bigint -> (bigint * bigint)
    default this.stepv_12 (p, g) =
        m_p <- p
        m_g <- g

        (* 
        g = 1 for B:
            g^b = 1 = B
            g^bA = 1
            s = 1 // on A's side
        *)

        (p, bigint 1)

    abstract member stepv_34: bigint -> bigint
    default this.stepv_34 A_ =
        A <- A_
        A_

    abstract member stepv_45: bigint -> bigint
    default this.stepv_45 B =
        assert (B = (bigint 1))
        B

    abstract member s_for_B: bigint
    default this.s_for_B = bigint 1

    abstract member stepv_56: seq<byte[]> * byte[] -> (byte array seq * byte[])
    default this.stepv_56 (cr, iv) =
        msg <- this.decrypt (cr, iv) this.s_for_B |> List.ofSeq
        msg
        |> Seq.concat
        |> bytes_String
        |> printf "MITM decrypt:\n %s\n"

        // must re-encrypt so that B can read it with g = 1
        // has done s = A^b = g^Ab = 1
        this.encrypt this.s_for_B msg

    abstract member stepv_67: seq<byte[]> * byte[] -> (byte array seq * byte[])
    default this.stepv_67 (cr, iv) =
        // re-encrypt for A
        let s = BigInteger.ModPow(m_g, A, m_p)
        this.encrypt s msg




type DHVarGroupMitm2 () =
    inherit DHVarGroupMitm ()

    override this.stepv_12 (p, g) =
        let (p_, g_) = base.stepv_12 (p, g)
        (p_, p_)

        (*
        B will do:
            B = g^b (mod p)
            B = p^b (mod p)
            B = 0

        A will do:
            s = B^a (mod p)
            s = 0
        *)

    override this.stepv_45 B =
        assert (B = (bigint 0))
        B

    override this.s_for_B = bigint 0

    override this.stepv_67 (cr, iv) =
        this.encrypt (bigint 0) this.Message


type DHVarGroupMitm3 () =
    inherit DHVarGroupMitm ()

    let mutable p1 = bigint 0
    let mutable s = bigint 0

    override this.stepv_12 (p, g) =
        let (p_, g_) = base.stepv_12 (p, g)
        p1 <- p_ - (bigint 1)
        (p_, p1)

        (*
        B will do:
            B = g^b (mod p)
            B = (p-1)^b (mod p)

            when b is even:
                B = 1
                B.s = A^b (mod p)
                A.s = B^a (mod p)
                A.s = 1

            when b is odd:
                B = p - 1
                A.s = B^a (mod p)
                A.s = (p-1)^a (mod p)

                when a is even:
                    A.s = 1

                when a is odd:
                    A.s = p - 1

        *)
    
    override this.stepv_45 B =
        assert ((B = (bigint 1)) || (B = p1))
        B

    override this.s_for_B = s

    member this.try_s (cr, iv) s_ =
        s <- s_
        try
            let a,b = base.stepv_56 (cr, iv)
            let a2 = Seq.cache a
            Seq.iter ignore a2 |> ignore
            Some ((a2, b), s_)
        with
            _ -> None

        

    override this.stepv_56 (cr, iv) =
        let a = this.try_s (cr, iv) (bigint 1)
        let b = this.try_s (cr, iv) p1
            
        Option.coalesce a b
        |> Option.get
        |> fst


let task35 () = 
    let run (m:DHVarGroupMitm) =
        let sa = new DHVarGroup()
        let sb = new DHVarGroup()

        sa.stepv1_a ()
        |> m.stepv_12
        |> sb.stepv2_b
        |> sa.stepv3_a
        |> sb.stepv4_b
        |> m.stepv_45
        |> sa.stepv5_a
        |> m.stepv_56
        |> sb.stepv6_b
        |> m.stepv_67
        |> sa.stepv7_a
        |> Seq.concat
        |> bytes_String
        |> printf "A side received:\n%s"
   
    run (new DHVarGroupMitm())
    run (new DHVarGroupMitm2())
    run (new DHVarGroupMitm3())


type SRPMessaging () =
    
    let g = bigint 2
    let k = bigint 3
    
    let mutable salt = bigint 0
    let mutable v = bigint 0
    let mutable a = bigint 0
    let mutable b = bigint 0
    let mutable u = bigint 0
    let mutable pass:string = ""


    let calc_x salt password = 
        let xH = sha256 (String.Format("{0}|{1}", salt, password) |> String_bytes)
        new BigInteger(xH) |> abs


    let calc_u A B =
        let uH = sha256 (String.Format("{0}|{1}", A, B) |> String_bytes)
        new BigInteger(uH) |> abs
    
    
    member val  A = bigint 0 with get, set
    member val  B = bigint 0 with get, set
    member this.N = g_p
    member this.b_
        with get () = b
    member this.u_
        with get () = u
        and set x = u <- x

    member this.a_ 
        with get () = a
        and set x = a <- x

    member this.salt_ 
        with get () = salt
        and set x = salt <- x

    member this.calc_x_nopass salt =
        calc_x salt pass
    
            
    member this.init_S (password:string) =
        b <- big_rand this.N
        salt <- big_rand this.N
        v <- BigInteger.ModPow(g, calc_x salt password, this.N)
        pass <- password

    member this.init_c (password:string) =
        a <- big_rand this.N
        pass <- password

    abstract member step1_c: unit -> (string * bigint)
    default this.step1_c () =
        this.A <- BigInteger.ModPow(g, a, this.N)
        let I = "pepa@pig.com"
        (I, this.A)

    abstract member step2_S: string * bigint -> bigint * bigint
    default this.step2_S (I, A_) =
        this.A <- A_
        this.B <- k * v + BigInteger.ModPow(g, b, this.N)
        u <- calc_u A_ this.B
        (salt, this.B)

    member this.calc_hmac (S:bigint) =
        let K = sha256 (S.ToByteArray())
        hmac_sha256 K (salt.ToByteArray())

    abstract member step3_c: bigint * bigint -> byte[]
    default this.step3_c (salt_:bigint, B) =
        u <- calc_u this.A B
        salt <- salt_
        let x = calc_x salt pass
        let S = BigInteger.ModPow(B - k * (modpow g x this.N), a + u * x, this.N)
        this.calc_hmac S

    abstract member step4_S: byte[] -> bool
    default this.step4_S (hm) =
        let S = modpow (this.A * (modpow v u this.N)) b this.N
        let hash = this.calc_hmac S
        hm = hash


let task36 () =
    let server = SRPMessaging()
    let client = SRPMessaging()
    server.init_S "kocka"
    client.init_c "kocka"

    client.step1_c ()
    |> server.step2_S
    |> client.step3_c
    |> server.step4_S
    


type InvalidSRPMessaging () =
    inherit SRPMessaging()

    override this.step1_c () =
        let I_, A_ = base.step1_c()
        this.A <- this.N
        (I_, this.A)

    override this.step3_c (salt, B) =
        let h = base.step3_c (salt, B)
        let K = sha256 ((new bigint(0)).ToByteArray())
        hmac_sha256 K (salt.ToByteArray())


let task37 () =
    let server = SRPMessaging()
    let client = InvalidSRPMessaging()
    server.init_S "kocka"
    client.init_c "unknown"

    client.step1_c ()
    |> server.step2_S
    |> client.step3_c
    |> server.step4_S


type SimpleSRP () =
    inherit SRPMessaging()

    member this.step2new_S (I, A_) =
        this.A <- A_
        this.B <- modpow g (this.b_) this.N
        this.u_ <- (big_rand ((bigint 1) <<< 128))
        (this.salt_, this.B, this.u_)

    member this.step3new_c (salt, B, u) =
        this.u_ <- u
        this.salt_ <- salt
        this.B <- B
        let x = this.calc_x_nopass salt
        let S = modpow this.B (this.a_ + this.u_ * x) this.N
        this.calc_hmac S


type SRPMitm (server:SimpleSRP) =
    let dict = System.IO.File.ReadAllLines(System.IO.Path.Combine(DATA_DIR, "wordsEn.txt"))

    let b = bigint 567  // pretend it's safe
    let B = modpow g b server.N
    let u = bigint 5647897
    let salt = bigint 1
    let mutable A = bigint 0

    let calc_x salt password = 
        let xH = sha256 (String.Format("{0}|{1}", salt, password) |> String_bytes)
        new BigInteger(xH) |> abs

    let calc_hmac (S:bigint) =
        let K = sha256 (S.ToByteArray())
        hmac_sha256 K (salt.ToByteArray())

    let crack_pwd hm =
        let N = server.N
        let try_pw pw =
            let x = calc_x salt pw
            let v = modpow g x N
            let S = modpow (A * (modpow v u N)) b N
            (calc_hmac S) = hm
        PSeq.find try_pw dict

    member this.step2new_S (I, A_) =
        A <- A_
        let (salt_, B_, u_) = server.step2new_S (I, A_)
        (salt, B, u)
        
    member this.step4_S hm =
        crack_pwd hm |> printf "pwd: %A\n"
        server.step4_S hm
                     

let task38 () =
    let server = SimpleSRP()
    let client = SimpleSRP()
    let eve = SRPMitm(server)
    server.init_S "table"
    client.init_c "table"

    client.step1_c ()
    |> eve.step2new_S
    |> client.step3new_c
    |> eve.step4_S

